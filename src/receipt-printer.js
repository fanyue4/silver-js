export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-
    let result = '==================== Receipt ====================\n';
    let sumPrice = 0;
    const resultFooter = '===================== Total =====================\n';

    if (barcodes.length === 0) {
      result += '\n\n' + resultFooter + `${sumPrice.toFixed(2)}`;
      return result;
    }

    const barcodesCount = {};
    barcodes.forEach(barcode => {
      barcodesCount[barcode] = (barcodesCount[barcode] || 0) + 1;
    });
    const sortedBarcodesCount = Object.entries(barcodesCount).sort();

    const selectedProducts = this.products.filter(product => barcodes.includes(product.barcode)).sort();
    if (selectedProducts.length < sortedBarcodesCount.length) {
      throw Error('Unknown barcode.');
    }

    for (let i = 0; i < selectedProducts.length; i++) {
      sumPrice += selectedProducts[i].price * sortedBarcodesCount[i][1];
      result += `${selectedProducts[i].name}`.padEnd(30) + `x${sortedBarcodesCount[i][1]}` + '        ' + `${selectedProducts[i].unit}\n`;
    }
    result += '\n' + resultFooter + `${sumPrice.toFixed(2)}`;

    return result;
    // --end->
  }
}
